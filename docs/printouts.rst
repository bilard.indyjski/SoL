.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   sab 12 lug 2014 16:55:55 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2014 Lele Gaifax
..

==================================================
 :mod:`sol.printouts` -- PDF printouts generators
==================================================

.. automodule:: sol.printouts
   :members:
