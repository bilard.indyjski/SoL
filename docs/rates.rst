.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   lun 03 mar 2014 12:44:55 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2014 Lele Gaifax
..

========================================
 :mod:`sol.models.rate` -- Player rates
========================================

.. automodule:: sol.models.rate

.. autoclass:: Rate
   :members:
