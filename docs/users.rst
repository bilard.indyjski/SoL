.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   ven 13 lug 2018 09:16:31 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2018 Lele Gaifax
..

================================
 :mod:`sol.models.user` -- User
================================

.. automodule:: sol.models.user

.. autoclass:: User
   :members:
