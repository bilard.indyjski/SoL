.. -*- coding: utf-8 -*-
.. :Project:   SoL
.. :Created:   dom 19 gen 2020, 09:55:46
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2020 Lele Gaifax
..

.. _gestione associazione club-utenti:

Gestione associazione utenti di un club
---------------------------------------

.. index::
   triple: Club; Utenti; Associazione

In SoL 4, un utente può creare nuovi *campionati*, *valutazioni* e *tornei* legati ai club di
cui è responsabile (che di solito sono quelli che ha creato lui stesso). In alcuni casi, in
particolare per le *federazioni*, può essere desiderabile permettere di farlo anche ad altri
utenti.

Premendo il tasto destro del mouse nella tabella di :ref:`gestione dei club <gestione club>`,
il responsabile (o l'amministratore) troveranno l'azione :guilabel:`Utenti` che apre questo
pannello:

.. figure:: utenticlub.png

   Gestione utenti di un club

Sono elencati tutti gli utenti :ref:`confermati <stato utente>`, con un *checkbox* a fianco di
ognuno che dice se l'utente è attualmente associato al club o meno: come detto, solo quelli
marcati potranno creare, ad esempio, un nuovo torneo collegato al club.
