# nixos-19.03-small Released on 2019-07-22
NIXPKGS_REV="a607a931f6f7db2ba97a9788d36192326c63c92f";
export NIX_PATH="nixpkgs=https://github.com/NixOs/nixpkgs-channels/archive/${NIXPKGS_REV}.tar.gz"
